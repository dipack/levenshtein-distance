import os
import sys

def levenshtein(str1: 'str', str2: 'str') -> 'int':
    l1, l2 = len(str1), len(str2)

    d = [[0 for _ in range(l2)] for _ in range(l1)]

    # Setting the first row and column to increasing values in case one of the strings is empty
    for idx in range(1, l1):
        d[idx][0] = idx

    for idx in range(1, l2):
        d[0][idx] = idx

    for oIdx in range(1, l2):
        for idx in range(1, l1):
            cost = 0
            if str1[idx] == str2[oIdx]:
                cost = 0
            else:
                cost = 1
            d[idx][oIdx] = min(d[idx - 1][oIdx] + 1, d[idx][oIdx - 1] + 1, d[idx - 1][oIdx - 1] + cost)
    return d[l1 - 1][l2 - 1] + 1

if __name__ == "__main__":
    args = sys.argv
    if len(args) != 3:
        print("Need to provide two strings to calculate their Levenshtein distance!")
        sys.exit(os.EX_NOINPUT)
    dist = levenshtein(args[1], args[2])
    print("Levenshtein distance between [{0}] and [{1}] is {2}".format(args[1], args[2], dist))

